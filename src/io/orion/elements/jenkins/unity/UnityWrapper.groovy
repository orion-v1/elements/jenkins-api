package io.orion.elements.jenkins.unity

import io.orion.elements.jenkins.IJenkinsBridge
import io.orion.elements.jenkins.ShellCommandBuilder

public class UnityWrapper implements Serializable
{
    private IJenkinsBridge _jenkins
    private String _path

    public UnityWrapper(IJenkinsBridge jenkins, String path)
    {
        _jenkins = jenkins
        _path = path
    }

    IJenkinsBridge getJenkinsBridge()
    {
        return _jenkins
    }

    ShellCommandBuilder createCommandBuilder()
    {
        return new ShellCommandBuilder()
            .setCommand(_path);
    }

    public UnityGenericCommand getGenericCommand()
    {
        return new UnityGenericCommand(this)
    }

    public UnityCommand getBuildProjectCommand()
    {
        return new UnityBuildProjectCommand(this)
    }

    public UnityEmptyCommand getEmptyCommand()
    {
        return new UnityEmptyCommand(this)
    }
}
