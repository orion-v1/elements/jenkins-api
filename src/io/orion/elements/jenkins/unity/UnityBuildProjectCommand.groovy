package io.orion.elements.jenkins.unity

import io.orion.elements.jenkins.ShellCommandBuilder

public class UnityBuildProjectCommand extends UnityCommand
{
    UnityBuildProjectCommand(UnityWrapper unity)
    {
        super(unity)
    }

    public def setProjectName(String value)
    {
        if (value != null) _args["--projectName"] = value
        else _args.remove("--projectName")
        return this
    }

    public def setBuildNumber(int value)
    {
        if (value >= 0) _args["--buildNumber"] = value
        else _args.remove("--buildNumber")
        return this
    }

    public def setBuildSuffix(String value)
    {
        if (value != null) _args["--buildSuffix"] = value
        else _args.remove("--buildSuffix")
        return this
    }

    public def setBuildReportPath(String value)
    {
        if (value != null) _args["--buildReportPath"] = value
        else _args.remove("--buildReportPath")
        return this
    }

    public def setExportMode(String value)
    {
        if (value != null) _args["--exportMode"] = value
        else _args.remove("--exportMode")
        return this
    }

    public def setKeystorePath(String value)
    {
        if (value != null) _args["--keystorePath"] = value
        else _args.remove("--keystorePath")
        return this
    }

    public def setKeystorePassword(String value)
    {
        if (value != null) _args["--keystorePassword"] = value
        else _args.remove("--keystorePassword")
        return this
    }

    public def setKeyAlias(String value)
    {
        if (value != null) _args["--keyAlias"] = value
        else _args.remove("--keyAlias")
        return this
    }

    public def setKeyPassword(String value)
    {
        if (value != null) _args["--keyPassword"] = value
        else _args.remove("--keyPassword")
        return this
    }

    public def setKeystoreCredentialsFile(String value)
    {
        if (value != null) _args["--keystoreCredentialsFile"] = value
        else _args.remove("--keystoreCredentialsFile")
        return this
    }

    public def setAppleDeveloperTeamId(String value)
    {
        if (value != null) _args["--appleTeamId"] = value
        else _args.remove("--appleTeamId")
        return this
    }

    public def setDevelopmentBuild(boolean value)
    {
        if (value) _args["--developmentBuild"] = null
        else _args.remove("--developmentBuild")
        return this
    }

    public def setEnableScriptDebugging(boolean value)
    {
        if (value) _args["--enableScriptDebugging"] = null
        else _args.remove("--enableScriptDebugging")
        return this
    }

    public def setEnableProfiler(boolean value)
    {
        if (value) _args["--enableProfiler"] = null
        else _args.remove("--enableProfiler")
        return this
    }

    public Map execute()
    {
        def jenkins = _unity.getJenkinsBridge()
        setExecuteMethod("Orion.Elements.Cli.CommandLine.BuildProject")
        
        if (getBuildTarget() == null)
        {
            jenkins.error("Build target is not specified.")
            return null;
        }

        String projectPath = getProjectPath()
        if (projectPath == null)
        {
            jenkins.error("Projects path is not specified.")
            return null;
        }

        ShellCommandBuilder cmd = createCommandBuilder()
        jenkins.sh(cmd.build())

        String reportPath = _args["--buildReportPath"]
        if (reportPath != null)
            return jenkins.readJson(file: reportPath)

        return null
    }
}