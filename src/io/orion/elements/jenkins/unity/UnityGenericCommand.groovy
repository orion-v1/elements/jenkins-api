package io.orion.elements.jenkins.unity

import io.orion.elements.jenkins.ShellCommandBuilder

public class UnityGenericCommand extends UnityCommand
{
    UnityGenericCommand(UnityWrapper unity)
    {
        super(unity)
    }

    public void execute()
    {
        ShellCommandBuilder cmd = createCommandBuilder()
        _unity.getJenkinsBridge().sh(cmd.build())
    }
}
