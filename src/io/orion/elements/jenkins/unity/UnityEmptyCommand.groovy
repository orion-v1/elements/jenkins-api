package io.orion.elements.jenkins.unity

import io.orion.elements.jenkins.ShellCommandBuilder

public class UnityEmptyCommand extends UnityCommand
{
    UnityEmptyCommand(UnityWrapper unity)
    {
        super(unity)
    }

    public void execute()
    {
        def jenkins = _unity.getJenkinsBridge()
        setExecuteMethod("Orion.Elements.Cli.CommandLine.Execute")
        
        ShellCommandBuilder cmd = createCommandBuilder()
        jenkins.sh(cmd.build())
    }
}
