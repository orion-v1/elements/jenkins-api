package io.orion.elements.jenkins.unityhub

import io.orion.elements.jenkins.IJenkinsBridge
import io.orion.elements.jenkins.ShellCommandBuilder
import io.orion.elements.jenkins.unity.UnityWrapper

public class UnityHubWrapper implements Serializable
{
    private IJenkinsBridge _jenkins
    private String _path

    public UnityHubWrapper(IJenkinsBridge jenkins, String path)
    {
        _jenkins = jenkins
        _path = path
    }

    IJenkinsBridge getJenkinsBridge()
    {
        return _jenkins
    }

    ShellCommandBuilder createCommandBuilder()
    {
        return new ShellCommandBuilder()
            .setCommand(_path)
            .addOption("--")
            .addOption("--headless")
    }

    @NonCPS
    def getMatchedVersion(Map versions, String preferredVersion)
    {
        // check preferredVersion
        if (versions.containsKey(preferredVersion))
            return preferredVersion

        // create selection
        def arr = [] 
        versions.each { item -> 
            def version = parseVersion(item.key)
            if (version != null)
                arr.add(version)
        }

        arr = arr.sort { va, vb ->
            int val = va.year - vb.year
            if (val == 0) val = va.major - vb.major
            if (val == 0) val = va.minor - vb.minor
            if (val == 0) val = va.revision - vb.revision
            return val
        }

        // match
        def version = parseVersion(preferredVersion)
        if (version.year != null) arr = arr.findAll { item -> item.year == version.year}
        if (version.major != null) arr = arr.findAll { item -> item.major == version.major}
        if (version.minor != null) arr = arr.findAll { item -> item.minor == version.minor}
        if (version.type != null) arr = arr.findAll { item -> item.type == version.type}
        
        // return result
        int len = arr.size()
        if (len == 0)
            return null;

        version = arr[len - 1]
        StringBuilder sb = new StringBuilder()
        sb.append(version.year)
        sb.append('.')
        sb.append(version.major)
        sb.append('.')
        sb.append(version.minor)
        sb.append(version.type)
        sb.append(version.revision)
        return sb.toString()
    }

    @NonCPS
    def parseVersion(String version)
    {
        def result = parseVersion(version, /(\d+)\.(\d+)\.(\d+)([a-z])(\d+)/)
        if (result != null) return result;

        result = parseVersion(version, /(\d+)\.(\d+)\.(\d+)([a-z])/)
        if (result != null) return result;

        result = parseVersion(version, /(\d+)\.(\d+)\.(\d+)/)
        if (result != null) return result;

        result = parseVersion(version, /(\d+)\.(\d+)/)
        if (result != null) return result;

        result = parseVersion(version, /(\d+)/)
        if (result != null) return result;
        
        return null
    }

    @NonCPS
    def parseVersion(String version, pattern)
    {
        def matcher = version =~ pattern
        
        if (!matcher.find())
            return null
            
        def data = matcher[0];
        if (data == null)
            return null

        int len = data.size()
        if (len < 2)
            return null

        def result = [:]
        if (len > 1) result.year = data[1].toInteger()
        if (len > 2) result.major = data[2].toInteger()
        if (len > 3) result.minor = data[3].toInteger()
        if (len > 4) result.type = data[4]
        if (len > 5) result.revision = data[5].toInteger()
        return result
    }

    public UnityHubEditorsCommand getUnityHubEditorsCommand()
    {
        return new UnityHubEditorsCommand(this)
    }

    public UnityWrapper getUnityWrapper(Map options)
    {
        if (options.version != null && !options.version.isEmpty())
        {
            _jenkins.echo("Resolving Unity Editor by specified version '${options.version}'...")
            return getUnityWrapperForVersion(options.version)
        }

        if (options.projectPath != null)
        {
            _jenkins.echo("Resolving Unity Editor by project path '${options.projectPath}'...")
            return getUnityWrapperForProject(options.projectPath)
        }

        _jenkins.error("Failed to get UnityWrapper. Required at least one option: version, projectPath.")
        return null
    }

    public UnityWrapper getUnityWrapperForVersion(String versionPattern)
    {
        Map versions = new UnityHubEditorsCommand(this)
            .setInstalled(true)
            .execute()

        String version = getMatchedVersion(versions, versionPattern);
        if (version == null)
        {
            _jenkins.error("Unity ${versionPattern} is not installed.")
            return null;
        }
        
        _jenkins.echo("Selected Unity ${version}")
        String path = versions[version];
        if (path == null)
        {
            _jenkins.error("Failed to get path to Unity ${version}.")
            return null;
        }

        return new UnityWrapper(_jenkins, path)
    }

    public UnityWrapper getUnityWrapperForProject(String projectPath)
    {
        def versionFile = "${projectPath}/ProjectSettings/ProjectVersion.txt"
        if (!_jenkins.fileExists(versionFile))
        {
            _jenkins.error("Failed to get Unity version from project at path '${projectPath}'")
            return null;
        }

        def data = _jenkins.readFile(versionFile)
        def version = nonCpsMatchString(data, /\d+\.\d+\.\d+[a-z]\d+/)
        if (version == null)
        {
            _jenkins.error("Failed to get Unity version from file '${versionFile}'")
            return null;
        }

        return getUnityWrapperForVersion(version)
    }

    @NonCPS
    private String nonCpsMatchString(data, pattern) 
    {
        def matcher = (data =~ pattern)
        if (matcher.find())
            return matcher[0]
            
        return null;
    }
}
