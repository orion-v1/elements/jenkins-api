package io.orion.elements.jenkins.xcode

import io.orion.elements.jenkins.Path
import io.orion.elements.jenkins.ShellCommandBuilder

// /Applications/Xcode.app/Contents/Developer/usr/bin/altool \
//      --validate-app \
//      -f <file> \
//      -t ios \
//      -u <username> \
//      -p <password>

public class XcodeUploadPackageCommand extends XcodeCommand
{
    private String _operation
    private boolean _suppressExceptions

    XcodeUploadPackageCommand(XcodeWrapper xcode)
    {
        super(xcode)
    }

    public XcodeUploadPackageCommand setOperation(String operation)
    {
        _operation = operation
        return this
    }

    public XcodeUploadPackageCommand setValidateOperation()
    {
        _operation = "--validate-app"
        return this
    }

    public XcodeUploadPackageCommand setUploadOperation()
    {
        _operation = "--upload-app"
        return this
    }
    
    public XcodeUploadPackageCommand setVerbose(boolean value)
    {
        if (value) _args["--verbose"] = null
        else _args.remove("--verbose")
        return this
    }

    public XcodeUploadPackageCommand setPackagePath(String value)
    {
        if (value != null) _args["-f"] = value
        else _args.remove("-f")
        return this
    }

    public XcodeUploadPackageCommand setPlatformType(String value)
    {
        if (value != null) _args["-t"] = value
        else _args.remove("-t")
        return this
    }

    public XcodeUploadPackageCommand setUsername(String value)
    {
        if (value != null) _args["-u"] = value
        else _args.remove("-u")
        return this
    }

    public XcodeUploadPackageCommand setPassword(String value)
    {
        if (value != null) _args["-p"] = value
        else _args.remove("-p")
        return this
    }
    
    public XcodeUploadPackageCommand setSuppressExceptions(boolean value)
    {
        _suppressExceptions = value
        return this
    }

    public def execute()
    {
        def rs = [:]

        try
        {
            def jenkins = _xcode.getJenkinsBridge()

            if (_operation == null || _operation.isEmpty())
                throw new Exception("Operation is not specified.")

            ShellCommandBuilder cmd = createCommandBuilder("${_xcode.getPath()}/Contents/Developer/usr/bin/altool", _operation)
            jenkins.sh(cmd.build())

            rs.success = true
        }
        catch (e)
        {
            rs.success = false
            if (!_suppressExceptions) throw e
        }

        return rs
    }
}
