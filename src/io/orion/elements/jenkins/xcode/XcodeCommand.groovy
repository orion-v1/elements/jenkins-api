package io.orion.elements.jenkins.xcode

import io.orion.elements.jenkins.ShellCommandBuilder

public class XcodeCommand implements Serializable
{
    protected XcodeWrapper _xcode
    protected Map _args = [:]
    protected Map _projectOptions = [:]

    XcodeCommand(XcodeWrapper xcode)
    {
        _xcode = xcode
    }

    ShellCommandBuilder createCommandBuilder(String command, String operation)
    {
        return new ShellCommandBuilder()
            .setCommand(command)
            .addOption(operation)
            .addOptions(_args, " ")
            .addOptions(_projectOptions, "=")
    }

    public def addOption(String option)
    {
        _args[option] = null
    }

    public def addOption(String option, String value)
    {
        if (value != null) _args[option] = value
        else _args.remove(option)
        return this
    }

    public def addProjectOption(String option, String value)
    {
        if (value != null) _projectOptions[option] = value
        else _projectOptions.remove(option)
        return this
    }
}
