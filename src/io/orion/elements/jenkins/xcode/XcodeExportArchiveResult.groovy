package io.orion.elements.jenkins.xcode

public class XcodeExportArchiveResult implements Serializable
{
    private XcodeWrapper _xcode
    private XcodeExportArchiveCommand _command
    private String _ipaPath;

    XcodeExportArchiveResult(XcodeWrapper xcode, XcodeExportArchiveCommand command)
    {
        _xcode = xcode
        _command = command
    }

    public String getIpaPath()
    {
        if (_ipaPath != null)
            return _ipaPath

        _ipaPath = _xcode.getJenkinsBridge().sh(true, "find \"${_command.getExportPath()}\" -type f -name '*.ipa' | head -n 1 | tr -d '\n'")
        return _ipaPath
    }

    public void moveIpaTo(String name)
    {
        String src = getIpaPath()
        if (src == null)
        {
            // TODO: error
            return
        }

        _ipaPath = null; // reset cached path

        _xcode.getJenkinsBridge().sh("mv \"${src}\" \"${name}\"")        
    }
}
