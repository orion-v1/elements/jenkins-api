package io.orion.elements.jenkins.xcode

import io.orion.elements.jenkins.Path
import io.orion.elements.jenkins.ShellCommandBuilder

// xcodebuild -exportArchive \
//     -allowProvisioningUpdates \
//     -archivePath ".artifacts/archive.xcarchive" \
//     -exportPath ".artifacts/adhoc" \
//     -exportOptionsPlist "export-options.plist"

public class XcodeExportArchiveCommand extends XcodeCommand
{
    private XcodeExportArchiveOptions _options

    XcodeExportArchiveCommand(XcodeWrapper xcode)
    {
        super(xcode)
    }

    private String getOptionsPath()
    {
        return Path.combine(_xcode.getJenkinsBridge().getWorkspaceTempPath(), "export-archive-options.plist")
    }

    public XcodeExportArchiveCommand setAllowProvisioningUpdates(boolean value)
    {
        if (value) _args["-allowProvisioningUpdates"] = null
        else _args.remove("-allowProvisioningUpdates")        
        return this
    }

    public XcodeExportArchiveCommand setArchivePath(String value)
    {
        if (value != null) _args["-archivePath"] = value
        else _args.remove("-archivePath")
        return this
    }

    public XcodeExportArchiveCommand setExportPath(String value)
    {
        if (value != null) _args["-exportPath"] = value
        else _args.remove("-exportPath")
        return this
    }

    public String getExportPath()
    {
        return _args["-exportPath"];
    }

    public XcodeExportArchiveCommand setProject(String value)
    {
        if (value != null) _args["-project"] = value
        else _args.remove("-project")
        return this
    }

    public XcodeExportArchiveCommand setScheme(String value)
    {
        if (value != null) _args["-scheme"] = value
        else _args.remove("-scheme")
        return this
    }

    public XcodeExportArchiveCommand setExportOptionsPlist(String value)
    {
        if (value != null) _args["-exportOptionsPlist"] = value
        else _args.remove("-exportOptionsPlist")
        return this
    }

    public XcodeExportArchiveOptions setupExportOptions()
    {
        if (_options == null)
        {
            _options = new XcodeExportArchiveOptions(this)
            setExportOptionsPlist(getOptionsPath())
        }
        
        return _options;
    }

    public XcodeExportArchiveResult execute()
    {
        ShellCommandBuilder cmd = createCommandBuilder("xcodebuild", "-exportArchive")

        // save package options
        def jenkins = _xcode.getJenkinsBridge()
        if (_options != null)
        {
            jenkins.writeFile(getOptionsPath(), _options.toString())
        }

        jenkins.sh(cmd.build())

        return new XcodeExportArchiveResult(_xcode, this)
    }
}
