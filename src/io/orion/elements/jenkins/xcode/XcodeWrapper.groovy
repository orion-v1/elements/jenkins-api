package io.orion.elements.jenkins.xcode

import io.orion.elements.jenkins.IJenkinsBridge
import io.orion.elements.jenkins.ShellCommandBuilder

public class XcodeWrapper implements Serializable
{
    private IJenkinsBridge _jenkins
    private String _path

    public XcodeWrapper(IJenkinsBridge jenkins)
    {
        _jenkins = jenkins
        _path = "/Applications/Xcode.app"
    }

    public XcodeWrapper(IJenkinsBridge jenkins, String path)
    {
        _jenkins = jenkins
        _path = path
    }

    IJenkinsBridge getJenkinsBridge()
    {
        return _jenkins
    }

    String getPath()
    {
        return _path
    }

    void appendXcprettyIfAvailable(ShellCommandBuilder cmd)
    {
        if (isXcprettyAvailable())
            cmd.addRawText("| xcpretty; exit \${PIPESTATUS[0]}")
    }

    public boolean isXcprettyAvailable()
    {
        String msg = _jenkins.sh(true, 'if [ -x "$(command -v xcpretty)" ]; then echo "Yes"; fi')
        return msg != null && !msg.isEmpty()
    }

    public void installProvisioningProfilesAtPath(String path)
    {
        _jenkins.sh """
            if [ ! -d "${path}" ]; then
                exit 0
            fi

            PROFILES=\$(find "$path" -name "*.mobileprovision" -type f)
            OUTPUT_DIR="\$HOME/Library/MobileDevice/Provisioning Profiles"
            if [ -z "\$PROFILES" ]; then
                exit 0
            fi

            mkdir -p "\$OUTPUT_DIR"
            while IFS= read -r PROFILE
            do
                PROFILE_UUID=`/usr/libexec/PlistBuddy -c 'Print UUID' /dev/stdin <<< \$(security cms -D -i "\${PROFILE}")`
                PROFILE_NAME="\$PROFILE_UUID.mobileprovision"
                echo "Installing \$PROFILE -> \$PROFILE_NAME"

                cp -f "\$PROFILE" "\$OUTPUT_DIR/\$PROFILE_NAME"
            done <<< "\$PROFILES"
        """
    }

    public XcodeArchiveCommand getArchiveCommand()
    {
        return new XcodeArchiveCommand(this)
    }

    public XcodeExportArchiveCommand getExportArchiveCommand()
    {
        return new XcodeExportArchiveCommand(this)
    }

    public XcodeUploadPackageCommand getUploadPackageCommand()
    {
        return new XcodeUploadPackageCommand(this)
    }
}
