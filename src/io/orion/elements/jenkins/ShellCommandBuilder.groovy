package io.orion.elements.jenkins

public class ShellCommandBuilder implements Serializable
{
    //protected Map _args = [:]
    private String _command
    private StringBuilder _arguments

    public static def quoteValueIfRequired(def value)
    {
        if (value instanceof String || value instanceof GString)
        {
            value = value.trim();
            if ((!value.startsWith("\"") || !value.endsWith("\"")) && value.contains(" ")) value = "\"${value}\""
        }

        return value
    }

    public ShellCommandBuilder setCommand(String value)
    {
        _command = Path.getMacOsExecutableFile(value)
        return this
    }

    public ShellCommandBuilder addRawText(String value)
    {
        if (_arguments == null)
            _arguments = new StringBuilder();

        _arguments.append(value)
        _arguments.append(" ")
        return this
    }

    public ShellCommandBuilder addOption(String option, def value, String valueSeparator)
    {
        if (option == null || option.isEmpty())
            return this

        if (_arguments == null)
            _arguments = new StringBuilder();

        _arguments.append(option)
        if (value != null)
        {
            _arguments.append(valueSeparator)
            _arguments.append(quoteValueIfRequired(value))
        }
        _arguments.append(" ")

        return this
    }

    public ShellCommandBuilder addOption(String option, def value)
    {
        return addOption(option, value, " ")
    }

    public ShellCommandBuilder addOption(String option)
    {
        return addOption(option, null, " ")
    }

    public ShellCommandBuilder addOptions(Map args, String valueSeparator)
    {
        if (args == null)
            return this

        args.each { addOption(it.key, it.value, valueSeparator) }
        return this;
    }

    public ShellCommandBuilder addOptions(Map args)
    {
        return addOptions(args, " ")
    }

    public String build()
    {
        if (_command == null)
            return null;

        String cmd = quoteValueIfRequired(_command);
        if (_arguments != null)
        {
            cmd = "${cmd} ${_arguments.toString()}"
        }

        return cmd
    }
}