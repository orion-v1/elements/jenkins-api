package io.orion.elements.jenkins.appbundle

import io.orion.elements.jenkins.IJenkinsBridge
import io.orion.elements.jenkins.ShellCommandBuilder

public class GooglePlayAppBundleWrapper implements Serializable
{
    private IJenkinsBridge _jenkins
    private String _path

    public GooglePlayAppBundleWrapper(IJenkinsBridge jenkins, String path)
    {
        _jenkins = jenkins
        _path = path
    }

    IJenkinsBridge getJenkinsBridge()
    {
        return _jenkins
    }

    ShellCommandBuilder createCommandBuilder()
    {
        return new ShellCommandBuilder()
            .setCommand("java")
            .addOption("-jar")
            .addOption("${_path}/bundletool.jar")
    }

    public BuildApksCommand getBuildApksCommand()
    {
        return new BuildApksCommand(this)
    }

    public ExtractUniversalApkCommand getExtractUniversalApkCommand()
    {
        return new ExtractUniversalApkCommand(this)
    }
}
