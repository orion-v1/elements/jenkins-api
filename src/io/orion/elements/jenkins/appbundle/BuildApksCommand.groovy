package io.orion.elements.jenkins.appbundle

import io.orion.elements.jenkins.ShellCommandBuilder

public class BuildApksCommand implements Serializable
{
    protected GooglePlayAppBundleWrapper _wrapper
    protected Map _args = [:]

    BuildApksCommand(GooglePlayAppBundleWrapper wrapper)
    {
        _wrapper = wrapper
    }

    public def setBundlePath(String value)
    {
        if (value != null) _args["--bundle"] = value
        else _args.remove("--bundle")
        return this
    }

    public def setOutputPath(String value)
    {
        if (value != null) _args["--output"] = value
        else _args.remove("--output")
        return this
    }

    public def setMode(String value)
    {
        if (value != null) _args["--mode"] = value
        else _args.remove("--mode")
        return this
    }

    public def setOverwrite(boolean value)
    {
        if (value != null) _args["--overwrite"] = null
        else _args.remove("--overwrite")
        return this
    }

    public def setKeystorePath(String value)
    {
        if (value != null) _args["--ks"] = value
        else _args.remove("--ks")
        return this
    }

    public def setKeystorePassword(String value)
    {
        if (value != null) _args["--ks-pass"] = value
        else _args.remove("--ks-pass")
        return this
    }

    public def setKeyAlias(String value)
    {
        if (value != null) _args["--ks-key-alias"] = value
        else _args.remove("--ks-key-alias")
        return this
    }

    public def setKeyPassword(String value)
    {
        if (value != null) _args["--key-pass"] = value
        else _args.remove("--key-pass")
        return this
    }

    public void execute()
    {
        ShellCommandBuilder cmd = _wrapper.createCommandBuilder()
            .addOption("build-apks")
            .addOptions(_args, "=")

        def jenkins = _wrapper.getJenkinsBridge()
        jenkins.sh(cmd.build())
    }
}
