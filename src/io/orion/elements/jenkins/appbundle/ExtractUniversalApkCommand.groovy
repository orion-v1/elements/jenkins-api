package io.orion.elements.jenkins.appbundle

public class ExtractUniversalApkCommand implements Serializable
{
    protected GooglePlayAppBundleWrapper _wrapper
    private BuildApksCommand _buildCommand
    private String _outputPath

    ExtractUniversalApkCommand(GooglePlayAppBundleWrapper wrapper)
    {
        _wrapper = wrapper
    }

    BuildApksCommand getBuildApksCommand()
    {
        if (_buildCommand == null)
            _buildCommand = _wrapper.getBuildApksCommand()
        return _buildCommand
    }

    public def setBundlePath(String value)
    {
        getBuildApksCommand().setBundlePath(value)
        return this
    }

    public def setOutputPath(String value)
    {
        _outputPath = value
        return this
    }

    public def setKeystorePath(String value)
    {
        getBuildApksCommand().setKeystorePath(value)
        return this
    }

    public def setKeystorePassword(String value)
    {
        getBuildApksCommand().setKeystorePassword(value)
        return this
    }

    public def setKeyAlias(String value)
    {
        getBuildApksCommand().setKeyAlias(value)
        return this
    }

    public def setKeyPassword(String value)
    {
        getBuildApksCommand().setKeyPassword(value)
        return this
    }

    public void execute()
    {
        def jenkins = _wrapper.getJenkinsBridge()        
        String tempPath = jenkins.getWorkspaceTempPath()
        String apksPath = "${tempPath}/package.apks"

        getBuildApksCommand()
            .setOutputPath(apksPath)
            .setMode("universal")
            .setOverwrite(true)
            .execute()

        jenkins.sh "unzip -o \"${apksPath}\" universal.apk -d \"${tempPath}\""
        jenkins.sh "mv \"${tempPath}/universal.apk\" \"${_outputPath}\""
    }
}
