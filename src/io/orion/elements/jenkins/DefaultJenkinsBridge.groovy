package io.orion.elements.jenkins

public class DefaultJenkinsBridge implements IJenkinsBridge 
{
    private Script _script

    public DefaultJenkinsBridge(script) 
    {
        _script = script
    }

    @Override
    public String sh(String command)
    {
        _script.sh(script: command, returnStdout: false)
    }

    @Override
    public String sh(boolean hideStdout, String command)
    {
        _script.sh(script: command, returnStdout: hideStdout)
    }

    @Override
    public void echo(String message)
    {
        _script.echo(message)
    }

    @Override
    public boolean fileExists(String path)
    {
        return _script.fileExists(path)
    }

    @Override
    public String readFile(options)
    {
        _script.readFile(options)
    }

    @Override
    public void writeFile(String path, String text)
    {
        _script.writeFile(file: path, text: text)
    }
    
    @Override
    public def readJson(options)
    {
        _script.readJSON(options)
    }

    @Override
    public void error(String message)    
    {
        _script.error message
    }

    @Override
    public String getWorkspacePath()
    {
        _script.pwd()
    }

    @Override
    public String getWorkspaceTempPath()
    {
        _script.pwd(tmp: true)
    }
}
