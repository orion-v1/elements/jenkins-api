package io.orion.elements.jenkins

public final class Path
{
    public static String combine(String... paths)
    {
        boolean separate = false;
        String result = ""
        for (path in paths) 
        {
            if (separate) result += "/"
            else separate = true

            result += path
        }

        return result
    }

    public static String getMacOsExecutableFile(String path)
    {
        def appName = path =~ /[^\/]+.app$/
        if (appName.find())
        {
            path += "/Contents/MacOS/${appName.group(0)[0..-5]}"
        }

        return path
    }
}
