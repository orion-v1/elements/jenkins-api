package io.orion.elements.jenkins

public class TestJenkinsBridge implements IJenkinsBridge 
{
    @Override
    public String sh(String command)
    {
        println ">> sh command:"
        println command
    }

    @Override
    public String sh(boolean hideStdout, String command)
    {
        println ">> sh command:"
        println command
    }

    @Override
    public boolean fileExists(String path)
    {
        println ">> fileExists (${path}):"
        return false
    }

    @Override
    public String readFile(options)
    {
        println ">> read file (${options.file}):"
        return ""
    }

    @Override
    public void writeFile(String path, String text)
    {
        println ">> write file (${path}):"
        println text
    }

    @Override
    public def readJson(options)
    {
        println ">> read json (${options}):"
        //TODO: json to Map
    }

    @Override
    public void error(String message)    
    {
        println ">> ERROR: ${message}"
    }

    @Override
    public void echo(String message)
    {
        println ">> echo:"
        println message
    }

    @Override
    public String getWorkspacePath()
    {
        "workspace_path"
    }

    @Override
    public String getWorkspaceTempPath()
    {
        "workspace_temp_path"
    }
}
