// Pipeline methods

def postBuildResultMessage(Map options)
{
    if (options.credentialsId == null)
    {
        echo "Missing credentialsId."
        return
    }

    if (options.ignoreBuildAborted && oriJenkins.isBuildAborted())
    {
        return
    }

    def channelId = options.channelId

    withCredentials([string(credentialsId: options.credentialsId, variable: 'TOKEN')])
    {
        // get channel id
        if (options.channelId == null && options.channelName != null)
        {
            def rs = getConversationsList()
            if (!rs.ok)
            {
                echo "Failed to get list of slack channels."
                return
            }

            def channel = rs.channels.find { ch -> ch.name == options.channelName }
            if (channel == null)
            {
                echo "Missing slack channel with name ${options.channelName}."
                return
            }

            // join channel
            rs = joinConversation(channel.id)
            if (!rs.ok)
            {
                echo "Failed to join channel ${options.channelName} (${channel.id})"
                return
            }

            channelId = channel.id
        }

        // get channel id by user
        if (options.channelId == null && options.userId != null)
        {
            // open conversation
            def rs = openDirectConversation(options.userId)
            if (!rs.ok)
            {
                echo "Failed to open conversation with user (${options.userId})"
                return
            }

            channelId = rs.channel.id
        }

        // get channel id by user name
        if (options.channelId == null && options.userName != null)
        {
            // get user id
            def rs = getUsersList()
            if (!rs.ok)
            {
                echo "Failed to get users list from slack."
                return
            }

            def user = rs.members.find { user -> user.name == options.userName }
            if (user == null)
            {
                echo "Missing user with name ${options.userName}"
                return
            }

            // open conversation
            rs = openDirectConversation(user.id)
            if (!rs.ok)
            {
                echo "Failed to open conversation with user ${user.name} (${user.id})"
                return
            }

            channelId = rs.channel.id
        }

        if (channelId == null)
        {
            echo "Missing slack id."
            return
        }

        // send message
        def message = createBuildResultMessage(options)
        def rs = postMessage(channelId, message.text, message.blocks)
        if (!rs.ok)
        {
            echo "Failed to send message to channel ${options.channelName} (${channelId})"
            echo rs.error
        }
    }

    return
}

def createBuildResultMessage(Map svc)
{
    def rs = [:]
    def blocks = []
    def buttons = []

    if (svc.test == true)
    {
        rs.text = "test message."
        rs.blocks = ""
        return rs
    }

    // Jenkins pipeline status
    if (oriJenkins.isBuildSucceeded())
    {
        rs.text = ":white_check_mark: Pipeline completed:\n${JOB_NAME} #${BUILD_ID}"
        blocks.add(createTextSectionJson(":white_check_mark: Pipeline <${BUILD_URL}|${JOB_NAME} #${BUILD_ID}> has been successfully completed."))
        buttons.add(createButtonActionJson(text: "Summary", url: BUILD_URL))
    }
    else if (oriJenkins.isBuildUnstable())
    {
        rs.text = ":warning: Pipeline completed but unstable: ${JOB_NAME} #${BUILD_ID}"
        blocks.add(createTextSectionJson(":warning: Pipeline <${BUILD_URL}|${JOB_NAME} #${BUILD_ID}> has been completed but marked as unstable."))
        buttons.add(createButtonActionJson(text: "Summary", url: BUILD_URL))
        buttons.add(createButtonActionJson(text: "Console", style: "danger", url: "${BUILD_URL}/console"))
    }
    else if (oriJenkins.isBuildAborted())
    {
        rs.text = ":heavy_minus_sign: Pipeline aborted:\n${JOB_NAME} #${BUILD_ID}"
        blocks.add(createTextSectionJson(":heavy_minus_sign: Pipeline <${BUILD_URL}|${JOB_NAME} #${BUILD_ID}> has been aborted."))
        buttons.add(createButtonActionJson(text: "Summary", url: BUILD_URL))
    }
    else if (oriJenkins.isBuildFailed())
    {
        rs.text = ":x: Pipeline failed: ${JOB_NAME} #${BUILD_ID}"
        blocks.add(createTextSectionJson(":x: Pipeline <${BUILD_URL}|${JOB_NAME} #${BUILD_ID}> has been failed."))
        buttons.add(createButtonActionJson(text: "Summary", url: BUILD_URL))
        buttons.add(createButtonActionJson(text: "Console", style: "danger", url: "${BUILD_URL}/console"))
    }
    else
    {
        rs.text = ":question: Pipeline completed:\n${JOB_NAME} #${BUILD_ID}"
        blocks.add(createTextSectionJson(":question: Pipeline <${BUILD_URL}|${JOB_NAME} #${BUILD_ID}> has been completed with unknown status."))
        buttons.add(createButtonActionJson(text: "Summary", url: BUILD_URL))
    }

    // services
    if (svc != null)
    {
        // Git
        if (svc.git != null)
        {
            String text = "*Git*"

            if (svc.git.branch != null && !svc.git.branch.isEmpty()) text += "\nBranch: ${svc.git.branch}"
            if (svc.git.commitId != null && !svc.git.commitId.isEmpty()) text += "\nCommit Id: ${svc.git.commitId}"

            blocks.add(createTextSectionJson(text))
        }

        // Diawi
        if (svc.diawi != null)
        {
            if (svc.diawi.status == 2000)
            {
                blocks.add(createTextSectionJson("*Diawi*\nBuild successfully uploaded.\nLink: <${svc.diawi.link}>"))
                buttons.add(createButtonActionJson(text: "Diawi: Install", style: "primary", url: "${svc.diawi.link}"))
                buttons.add(createButtonActionJson(text: "Diawi: Show QR", url: "https://www.diawi.com/qrcode/link/${svc.diawi.hash}"))
            }
            else
            {
                String text = svc.diawi.message != null && !svc.diawi.message.isEmpty() 
                    ? "*Diawi*\nUploading failed.\n${svc.diawi.message}"
                    : "*Diawi*\nUploading failed."

                blocks.add(createTextSectionJson(text))
            }
        }

        // Apple AppStore
        if (svc.appleAppStore != null)
        {
            String text = svc.appleAppStore.success
                ? "*Apple AppStore*\nBuild successfully uploaded."
                : "*Apple AppStore*\nUploading failed."

            blocks.add(createTextSectionJson(text))
        }
    }

    // Make result
    // add blocks
    String json = ""
    boolean firstBlock = true
    blocks.each { block -> 
        if (firstBlock)
        {
            firstBlock = false
        }
        else 
            json += ','

        json += block
    }

    // add buttons
    firstBlock = true
    json += """,{"type":"actions","elements":["""
    buttons.each { block -> 
        if (firstBlock)
        {
            firstBlock = false
        }
        else 
            json += ','

        json += block
    }
    json += "]}"

    // return result
    rs.blocks = json
    return rs
}

// API methods

def getConversationsList()
{
    String json = sh (returnStdout: true, script:"""
        curl -X POST https://slack.com/api/conversations.list \
            -H "Content-Type: application/x-www-form-urlencoded" \
            -d "token=\$TOKEN&types=public_channel"
    """)

    return readJSON(text: json)
}

def openDirectConversation(String userId)
{
    String json = sh (returnStdout: true, script:"""
        curl -X POST https://slack.com/api/conversations.open \
            -H "Authorization: Bearer \$TOKEN" \
            -H "Content-Type: application/json" \
            -d '{"users":"${userId}"}'
    """)

    return readJSON(text: json)
}

def joinConversation(String channelId)
{
    String json = sh (returnStdout: true, script:"""
        curl -X POST https://slack.com/api/conversations.join \
            -H "Authorization: Bearer \$TOKEN" \
            -H "Content-Type: application/json" \
            -d '{"channel":"${channelId}"}'
    """)

    return readJSON(text: json)
}

def getUsersList()
{
    String json = sh (returnStdout: true, script:"""
        curl -X POST https://slack.com/api/users.list \
            -H "Content-Type: application/x-www-form-urlencoded" \
            -d "token=\$TOKEN"
    """)

    return readJSON(text: json)
}

def postMessage(String channelId, String text)
{
    String json = sh (returnStdout: true, script:"""
        curl -X POST https://slack.com/api/chat.postMessage \
            -H "Authorization: Bearer \$TOKEN" \
            -H "Content-Type: application/json" \
            -d '{"channel":"${channelId}", "text":"${text}"}'
    """)

    return readJSON(text: json)
}

def postMessage(String channelId, String text, String blocks)
{
    String json = sh (returnStdout: true, script:"""
        curl -X POST https://slack.com/api/chat.postMessage \
            -H "Authorization: Bearer \$TOKEN" \
            -H "Content-Type: application/json" \
            -d '{"channel":"${channelId}", "text":"${text}", "blocks":[${blocks}]}'
    """)

    return readJSON(text: json)
}


String createButtonActionJson(Map options)
{
    // {
    //     "text": {
    //         "type": "plain_text",
    //         "emoji": true,
    //         "text": "Summary"
    //     },
    //     "style": "primary",
    //     "url": "${BUILD_URL}"
    // }

    String json = "{\"type\":\"button\""

    if (options.text)
        json += ""","text":{"type":"plain_text","emoji":true,"text":"${options.text}"}"""    

    if (options.style != null)
        json += ",\"style\":\"${options.style}\""
    
    if (options.url != null)
        json += ",\"url\":\"${options.url}\""

    json += "}"
    return json;
}

String createTextSectionJson(String text)
{
    // {
    //     "type": "section",
    //     "text": {
    //         "type": "mrkdwn",
    //         "text": "${rs.text}"
    //     }
    // }

    return """{"type":"section","text":{"type":"mrkdwn","text":"${text}"}}"""
}

String createDividerJson()
{
    return """{"type":"divider"}"""
}
